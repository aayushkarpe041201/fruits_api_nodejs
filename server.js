///// Main server ///

import express from "express";
import colors from "colors";
import dotenv from "dotenv";

import fruitsRoute from "./routes/fruitsRoute.js";

dotenv.config();
const PORT = process.env.PORT || 5000;

const app = express();
app.use(express.json());

app.get("/", (req, res) => {
  res.send("API is running");
});

app.use("/api/fruits", fruitsRoute);

app.listen(PORT, () => {
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.bold.yellow
  );
});
