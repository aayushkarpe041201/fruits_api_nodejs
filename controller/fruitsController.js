import asyncHandler from "express-async-handler";

import fruits from "../data/fruits.js";

/**
 * @description     Get all fruits info
 * @route           GET /api/fruits
 * @access          public
 */

const getFruitsInfoSorted = async (req, res) => {
  // This will return a sorted list of fruits object based on color

  // sorting algo
  const sortedListOfFruits = fruits.sort((a, b) => {
    if (a.color < b.color) {
      return -1;
    }
    if (a.color > b.color) {
      return 1;
    }
    return 0;
  });

  // respons
  res.json(sortedListOfFruits);
};

export { getFruitsInfoSorted };
