import express from "express";

import { getFruitsInfoSorted } from "../controller/fruitsController.js";

const router = express.Router();

router.route("/").get(getFruitsInfoSorted);

export default router;
