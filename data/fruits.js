import { v4 as uuidv4 } from "uuid";

const fruits = [
  {
    id: uuidv4(),
    name: "apple",
    color: "red",
  },
  {
    id: uuidv4(),
    name: "strawberries",
    color: "red",
  },
  {
    id: uuidv4(),
    name: "watermelon",
    color: "red",
  },
  {
    id: uuidv4(),
    name: "bananas",
    color: "yellow",
  },
  {
    id: uuidv4(),
    name: "grapefruit",
    color: "yellow",
  },
  {
    id: uuidv4(),
    name: "oranges",
    color: "orange",
  },
  {
    id: uuidv4(),
    name: "blackberries",
    color: "purple",
  },
  {
    id: uuidv4(),
    name: "plums",
    color: "purple",
  },
  {
    id: uuidv4(),
    name: "blueberries",
    color: "purple",
  },
  {
    id: uuidv4(),
    name: "kiwi",
    color: "green",
  },
  {
    id: uuidv4(),
    name: "green apples",
    color: "green",
  },
  {
    id: uuidv4(),
    name: "green grapes",
    color: "green",
  },
  {
    id: uuidv4(),
    name: "White peaches",
    color: "white",
  },
  {
    id: uuidv4(),
    name: "pears",
    color: "white",
  },
];


export default fruits;